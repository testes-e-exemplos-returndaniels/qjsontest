#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include <QFile>
#include <QDebug>

void escreve_aluno();
void ler_aluno();

int main()
{
    escreve_aluno();
    ler_aluno();
    return 0;
}
/* Escrever arquivo .json */
void escreve_aluno(){
    QJsonObject aluno; // Cria objeto do tipo QJsonObject
    //insere dados no QJsonObject
    aluno.insert("nome", "Lissandra");
    aluno.insert("idade", 18);
    aluno.insert("local", "ufrj");

    QFile file("teste.json"); //cria objeto QFile

    if (file.open(QIODevice::WriteOnly)) {
        // se conseguir abrir o arquivo
        QJsonDocument doc(aluno); //Cria um QJsonDocument a partir de um QJsonObject
        file.write(doc.toJson()); //escreve o arquivo .json
    }
}
/* Leitura do arquivo .json */
void ler_aluno(){
    QFile loadFile("teste.json"); //cria objeto QFile

    if (loadFile.open(QIODevice::ReadOnly)) {
        // se conseguir abrir o arquivo
        QByteArray saveData = loadFile.readAll(); //ler arquivo, byte a byte
        QJsonDocument loadDoc(QJsonDocument::fromJson(saveData)); // carrega o arquivo num QJsonDocument
        QJsonObject obj = loadDoc.object(); //carrega QJsonDocument num QJsonObject
        //exibe elementos
        qDebug() << obj.value("nome").toString();
        qDebug() << obj.value("idade").toInt();
        qDebug() << obj.value("local").toString();
    }
}
